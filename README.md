# NachOS assignments: Multi-programming


## Goal

- Implement page table in NachOS
    - Modify its memory menagement code to make NachOS support multi-programming.


**Please refer to the [report](./MP2_report.pdf) for more details.**

## Appendix: What is NachOS?

> *NachOS* is instructional software for teaching undergraduate, and potentially graduate, level operating systems courses.  
> Website: https://homes.cs.washington.edu/~tom/nachos/
